import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:login_app_flutter/DataKemiskinan/Grafik.dart';
import 'package:login_app_flutter/home.dart';

import 'Grafik.dart';

class DataPage extends StatefulWidget {
  @override
  _DataPageState createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {
  double _positionX = 0.0;
  double _minPositionX = -100.0;
  double _maxPositionX = 100.0;

  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Container(
      width: double.infinity,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/assets/Tabel Data.png'),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              height: 881.10 * fem,
              child: Stack(
                children: [
                  Positioned(
                    left: 2 * fem,
                    top: 20 * fem,
                    child: Container(
                      width: 122 * fem,
                      height: 117 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            child: Align(
                              child: SizedBox(
                                width: 90 * fem,
                                height: 90 * fem,
                                child: Image.asset(
                                  "images/assets/data1.png",
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 115,
                    top: 60,
                    child: Text(
                      'Data Kemiskinan',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Positioned(
                    left: 30 * fem,
                    top: 160 * fem,
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DataPage()),
                              );
                            },
                            child: Text(
                              'Tabel Data',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                              selectionColor: Colors.lightBlue,
                            ),
                            style: ElevatedButton.styleFrom(
                                primary: Color.fromARGB(255, 212, 212, 212),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )),
                          ),
                          SizedBox(width: 10),
                          Container(
                            width: 100 * fem,
                            height: 37 * fem,
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Grafik(),
                                  ),
                                );
                              },
                              child: Text(
                                'Grafik',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                ),
                                selectionColor: Colors.lightBlue,
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: Color.fromARGB(255, 212, 212, 212),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5, 205, 5, 0),
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                      height: 440,
                      width: 1500,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.deepPurple,
                                width: 0,
                              ),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Image.asset(
                              "images/assets/Group 12.png",
                              width: 1000,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 795,
                    left: 0.5,
                    child: Container(
                      width: 430 * fem,
                      height: 60 * fem,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => HomePage()),
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                        ),
                        child: Image.asset("images/assets/Group 13.png"),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),

        // ),
        // Positioned(
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       ElevatedButton(
        //         onPressed: () {
        //           signOut().then((value) {
        //             // Navigasi ke halaman login setelah logout berhasil
        //             Navigator.pushReplacement(
        //               context,
        //               MaterialPageRoute(builder: (context) => loginpage()),
        //             );
        //           });
        //         },
        //         child: Text("Logout"),
        //       ),
        //     ],
        //   ),
        // ),
      ),
    );
  }
}
