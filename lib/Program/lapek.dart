import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/material.dart';
import 'package:login_app_flutter/Program/program.dart';
import 'package:login_app_flutter/home.dart';

class lapek extends StatefulWidget {
  @override
  State<lapek> createState() => _lapekState();
}

class _lapekState extends State<lapek> {
  double _positionX = 0.0;
  double _minPositionX = -100.0;
  double _maxPositionX = 100.0;

  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Container(
      width: double.infinity,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/assets/bgpeta.png'),
          ),
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Container(
            width: double.infinity,
            height: 881.10 * fem,
            child: Stack(children: [
              Positioned(
                left: 2 * fem,
                top: 20 * fem,
                child: Container(
                  width: 122 * fem,
                  height: 117 * fem,
                  child: Stack(
                    children: [
                      Positioned(
                        child: Align(
                          child: SizedBox(
                            width: 90 * fem,
                            height: 90 * fem,
                            child: Image.asset(
                              "images/assets/logolapek.png",
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 115,
                top: 60,
                child: Text(
                  'Lapangan Pekerjaan',
                  style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.black,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(5, 200, 5, 100),
                child: Container(
                  margin: const EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                  height: 550,
                  width: 1500,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.deepPurple,
                            width: 0,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Image.asset(
                          "images/assets/tblapek.png",
                          width: 1400,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 795,
                left: 0.5,
                child: Container(
                  width: 430 * fem,
                  height: 50 * fem,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                    ),
                    child: Image.asset("images/assets/Group 13.png"),
                  ),
                ),
              ),
            ]),
          ),
        ]),
      ),
    );
  }
}
