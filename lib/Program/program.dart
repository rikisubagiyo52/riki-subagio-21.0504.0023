import 'package:flutter/material.dart';
import 'package:login_app_flutter/Program/lapek.dart';
import 'package:login_app_flutter/Program/pendidikan.dart';
import 'package:login_app_flutter/home.dart';

class program extends StatefulWidget {
  @override
  _programState createState() => _programState();
}

class _programState extends State<program> {
  double _positionX = 0.0;
  double _minPositionX = -100.0;
  double _maxPositionX = 100.0;

  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Container(
      width: double.infinity,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/assets/bgprog.png'),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              height: 881.10 * fem,
              child: Stack(
                children: [
                  Positioned(
                    left: 2 * fem,
                    top: 20 * fem,
                    child: Container(
                      width: 122 * fem,
                      height: 117 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            child: Align(
                              child: SizedBox(
                                width: 90 * fem,
                                height: 90 * fem,
                                child: Image.asset(
                                  "images/assets/program.png",
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 120,
                    top: 60,
                    child: Text(
                      'Program',
                      style: TextStyle(
                        fontSize: 27.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Positioned(
                    left: 21 * fem,
                    top: 350 * fem,
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => lapek()),
                              );
                            },
                            child: Image.asset("images/assets/Frame 41.png"),
                            style: ElevatedButton.styleFrom(
                              primary: Color.fromARGB(255, 255, 255, 255),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 21 * fem,
                    top: 470 * fem,
                    child: Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => pendidikan()),
                              );
                            },
                            child: Image.asset("images/assets/Frame 37.png"),
                            style: ElevatedButton.styleFrom(
                              primary: Color.fromARGB(255, 255, 255, 255),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 795,
                    left: 0.5,
                    child: Container(
                      width: 430 * fem,
                      height: 60 * fem,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => HomePage()),
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                        ),
                        child: Image.asset("images/assets/Group 13.png"),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
